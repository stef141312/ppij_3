﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Hardware.Camera2;

namespace AugmentedReality
{
    [Activity(Label = "AugmentedReality", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        Context _context;
        TextureView _textureView;
        CameraManager _cameraManager;

        TextView _textView;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            _context = ApplicationContext;
            //_textureView = new TextureView(_context);
            _textView = new TextView(_context);

            _cameraManager = (CameraManager)_context.GetSystemService(CameraService);
            var cameraIdList = _cameraManager.GetCameraIdList();
            foreach(string s in cameraIdList)
            {
                _textView.Text += s + "\n";
                _textView.Text += _cameraManager.GetCameraCharacteristics(s)+"\n";
            }

            SetContentView(_textView);
        }
    }
}

